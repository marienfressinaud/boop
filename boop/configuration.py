import os
import yaml
import uuid

from boop.environment import Environment


def load(configuration_path, environment, output_path):
    configuration = {
        "SITE_URL": "filesystem",
        "SITE_TITLE": "A website",
        "SITE_TIMEZONE": "UTC",
        "SITE_BLOG_SLUG": "blog",
        "SITE_ENVIRONMENT": environment.name.lower(),
        "SITE_WEBSUB_HUB": "",
    }

    if os.path.exists(configuration_path):
        with open(configuration_path) as conf_file:
            conf_data = yaml.safe_load(conf_file)
        for key, value in conf_data.items():
            if key == "development":
                continue
            site_key = f"SITE_{key.upper()}"
            configuration[site_key] = value

        if environment == Environment.DEVELOPMENT and "development" in conf_data:
            for key, value in conf_data["development"].items():
                site_key = f"SITE_{key.upper()}"
                configuration[site_key] = value

    if "SITE_UUID" not in configuration:
        the_uuid = str(uuid.uuid5(uuid.NAMESPACE_URL, configuration["SITE_URL"]))
        configuration["SITE_UUID"] = the_uuid
        print(
            "Warning: you should set a uuid in your configuration file.",
            f"Here’s one for you: {the_uuid}",
        )

    if "SITE_AUTHOR" not in configuration:
        configuration["SITE_AUTHOR"] = f"{configuration['SITE_TITLE']}'s author"
    if configuration["SITE_URL"] == "filesystem":
        configuration["SITE_URL"] = f"file://{os.path.abspath(output_path)}/"

    return configuration
