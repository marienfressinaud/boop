import os
import uuid
import re
import yaml


class Page:
    def __init__(self, slug, page_content, configuration={}):
        # We initialize the page meta variables which will be accessible in the
        # template
        self.meta = {}
        self.meta.update(configuration)
        self.meta["PAGE_SLUG"] = slug
        self.meta["PAGE_TEMPLATE"] = "page"
        yaml_conf, page_content = extract_yaml_configuration(page_content)
        for key, value in yaml_conf.items():
            article_key = f"PAGE_{key.upper()}"
            self.meta[article_key] = value
        self.meta["PAGE_CONTENT"] = page_content

    def slug(self):
        return self.meta["PAGE_SLUG"]

    def title(self):
        return self.meta.get("PAGE_TITLE", "A page")

    def url(self):
        site_url = self.meta.get("SITE_URL", "")
        template_name = self.template()
        if "." in template_name:
            extension = os.path.splitext(template_name)[1]
        else:
            extension = ".html"

        return f"{site_url}{self.slug()}{extension}"

    def uuid(self):
        return str(uuid.uuid5(uuid.NAMESPACE_URL, self.url()))

    def content(self):
        return self.meta["PAGE_CONTENT"]

    def template(self):
        return self.meta["PAGE_TEMPLATE"]

    def is_private(self):
        return self.meta.get("PAGE_PRIVATE", False)

    def set_articles(self, articles):
        articles.sort(key=lambda article: article.date(), reverse=False)
        self.meta["ARTICLES"] = articles

    def set_feed(self, feed):
        self.meta["FEED"] = feed


YAML_FRONT_MATTER_REGEX = re.compile("(---\n.*\n)(---\n)(.*)", re.M | re.S)


def extract_yaml_configuration(content):
    match = YAML_FRONT_MATTER_REGEX.match(content)
    if match:
        yaml_conf = yaml.safe_load(match.group(1))
        return yaml_conf, match.group(3)
    else:
        return {}, content
