# Manage static files

You can create a `static/` folder in order to store all your <abbr>CSS</abbr>,
JavaScript, images, etc. The files in this folder will be copied under the
`_site/` folder by preserving the folders structure.

The most basic example is to write your <abbr>CSS</abbr> code in `static/style.css`.
The file will be then copied as `_site/style.css`. To link this file in your
<abbr>HTML</abbr> code, add this in your `<head>` element:

```html
<link rel="stylesheet" href="{{ SITE_URL }}style.css">
```

If you want to illustrate an article with an image, put it in the `static/`
folder as well (e.g. `static/images/illustration1.jpg`) and refer to it in your
Markdown article:

```markdown
![A description of the image](images/illustration1.jpg)
```
