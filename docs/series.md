# Create series

Boop! allows you to group articles in thematic series. A serie has its own page
and feed.

First, create a new folder under `articles/`:

```console
my-website$ mkdir articles/my-serie
```

Then, let's write two articles:

```markdown
---
title: First article of the serie
date: 2019-02-24 17:00
---

This is my first article.
```

```markdown
---
title: Second article of the serie
date: 2021-10-05 23:00
---

This is my second article.
```

If you call Boop!, you'll notice nothing different than before.

To transform this folder into a serie, you'll have to create a `serie.html`
file. This is a normal page, so it's generated with the `page` template by
default. We'll create a new `serie` template though. Let's write a
`articles/my-serie/serie.html` file:

```html
---
title: My serie
template: serie
---

<div>
    This is the main page of my serie!
</div>
```

And a `templates/serie.html` template:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>{{ PAGE_TITLE }}</title>
    </head>

    <body>
        <h1>Serie: {{ PAGE_TITLE }}</h1>

        {{ PAGE_CONTENT }}

        <p>
            The serie articles:
        </p>

        <ol>
            {% for article in ARTICLES %}
                <li>
                    <a href="{{ article.url() }}">{{ article.title() }}</a>
                </li>
            {% endfor %}
        </ol>
    </body>
</html>
```

Now, if you call the `boop.py` command, you'll notice two things: a new page
`_site/serie/my-serie.html` and a feed `_site/feeds/my-serie.atom.xml`. The
page will contain the small paragraph we wrote in the serie page, and the list
of articles from the template. Obviously, the feed contains only the articles
from the serie.

## Permanent `serie` template variables

Remember that a `serie` template is based on the `page` template, so it has
access [to the same variables](/docs/pages.md). It has access to two additional
variables:

- `ARTICLES`: the list of articles of the serie
- `FEED`: the feed of the serie

## Accessing series in other templates

Series are accessible in the `blog` template with the `SERIES` variable.

You also can access the serie of an article in the `article` templates via the
`ARTICLE_SERIE` variable.

The two most interesting functions to use on series objects are `title()` and
`url()` (you can find more in [the `boop/page.py` file](/boop/page.py)).

## Exclude articles from a serie

Sometimes, you may want to write an article in a serie folder, but to keep it
private. You can do it by declaring the following variable: `private: true`.

Remember this variable will also exclude the article from [the blog
page](/docs/blog.md).
