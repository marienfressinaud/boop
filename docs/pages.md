# Write pages

You might find yourself limited by writing only articles. Boop! allows you to
create additional pages by creating <abbr>HTML</abbr> files under a `pages/`
folder.

```console
$ mkdir pages/
$ echo '<html><body>My page</body></html>' > pages/my-page.html
$ boop.py
Warning: you should set a uuid in your configuration file. Here’s one for you: d836fc75-3412-5caf-91e2-2fad31343882
Written page: file:///path/to/my-website/_site/index.html
Written page: file:///path/to/my-website/_site/my-page.html
Written feed: file:///path/to/my-website/_site/feeds/all.atom.xml
Written article: file:///path/to/my-website/_site/my-article.html
Generated in 0.00s
Boop!
```

Please note that `pages/` folder only accepts <abbr>HTML</abbr> files. Unlike
the articles, Boop! will preserve the subfolders structure for pages (e.g.
`pages/foo/my-page.html` will be generated as `_site/foo/my-page.html` and NOT
as `_site/my-page.html`). If you find it's weird that articles and pages and
handled differently, I'll not argue with you (but past me had some good reasons
I can't remember :))

## Template

You can also define a generic page template in `templates/page.html`.
The file's content is accessible through the `PAGE_CONTENT` variable.

```console
$ echo '<html><body>{{ PAGE_CONTENT }}</body></html>' > templates/page.html
$ echo 'My page' > pages/my-page.html
$ boop.py
$ cat _site/my-page.html
<html><body>My page</body></html>
```

Please note since the index page is considered as a "normal" page, this template
**also applies to the index page**.

## Declaring variables

You can define Yaml variables in the pages that will be used in the template
then. The variables are uppercased and prepended by `PAGE_`. For instance, in a
`pages/my-page.html` file:

```html
---
title: My page
---
<div>
    This is the content of my page
</div>
```

And in the `templates/page.html` file:

```html
<html>
    <head>
        <title>{{ PAGE_TITLE }}</title>
    </head>
    <body>
        <h1>{{ PAGE_TITLE }}</h1>

        {{ PAGE_CONTENT }}
    </body>
</html>
```

This will generate the `_site/my-page.html` file:

```html
<html>
    <head>
        <title>My page</title>
    </head>
    <body>
        <h1>My page</h1>

        <div>
            This is the content of my page
        </div>
    </body>
</html>
```

You can change the template file by setting a `template` variable.

## Permanent `page` template variables

You'll always be able to access the following variables (as well as your own
variables):

- `PAGE_SLUG`: the slug of the page
- `PAGE_TEMPLATE`: the name of its template
- `PAGE_CONTENT`: the content of the page
