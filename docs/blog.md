# Create an index of your articles

You'll probably want to list the articles of your blog on a page. For that, you
need to create a `templates/blog.html` template. This template has access to an
`ARTICLES` variable:

```html
<html>
    <head>
        <title>Blog</title>
    </head>
    <body>
        <ul>
            {% for article in ARTICLES %}
                <li>
                    <a href="{{ article.url() }}">{{ article.title() }}</a>
                    ({{ article.date().strftime("%d %b %Y") }})
                </li>
            {% endfor %}
        </ul>
    </body>
</html>
```

This will generate a `_site/blog.html` page.

## The Article model

The `Article` model has several public methods:

- `url()`: the full <abbr>URL</abbr> of the article
- `title()`: the title of the article
- `content()`: the article's content in HTML
- `author()`: the author of the article
- `date()`: the publication date
- `update()`: the last update date
- `uuid()`: a unique identifier
- `slug()`: the name of the generated file, without its extension

You can also access the meta variables with `article.meta["ARTICLE_VARIABLE"]`.

To find more, have a look at [the `boop/article.py` file](/boop/article.py).

## Exclude articles from the blog

There are two special variables to exclude your articles from the blog page.
They are set in [the articles YAML section](/docs/articles.md).

`blog: false` will exclude the article from the blog and feed.

`private: true` will exclude the article from the same list, but also from the
series.

## Blog <abbr>URL</abbr>

If you want to change the <abbr>URL</abbr> of the blog page, you can override
the slug by setting a [`blog_slug` configuration variable](/docs/configuration.md).

For instance, you might want to use this page as the home page of your website:

```yaml
blog_slug: "index"
```
