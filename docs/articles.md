# Write articles

In Boop!, articles are written in Markdown and they live under the `articles/`
folder. Before writing articles, we'll need to create an `article` template
first. A template is a file containing the <abbr>HTML</abbr> layout of our
future pages. Let's start with a basic template:

```console
my-website$ mkdir templates
my-website$ echo '<html><body>{{ ARTICLE_CONTENT }}</body></html>' > templates/article.html
```

We can now write our first article:

```console
my-website$ mkdir articles
my-website$ echo '# My first article' > articles/my-article.md
my-website$ boop.py
Warning: you should set a uuid in your configuration file. Here’s one for you: d836fc75-3412-5caf-91e2-2fad31343882
Written page: file:///path/to/my-website/_site/index.html
Written feed: file:///path/to/my-website/_site/feeds/all.atom.xml
Written article: file:///path/to/my-website/_site/my-article.html
Generated in 0.08s
Boop!
```

If you open the generated source code, you'll notice that two things happened:

```console
my-website$ echo _site/my-article.html
<html><body><h1>My first article</h1></body></html>
```

First, the `my-article.md` Markdown file became a `.html` file under the
`_site/` folder. Secondly, its (transformed) content has been inserted in the
template file we created earlier.

Please note that `articles/` folder only accepts Markdown files. You can
organize your articles in subfolders, but Boop! will flatten the structure
(e.g. `articles/foo/my-article.md` will be generated as `_site/my-article.html`
and NOT as `_site/foo/my-article.html`).

You can open the article in your browser with:

```console
my-website$ xdg-open _site/my-article.html
```

## Declaring variables

You can (and you should) define variables in your articles via a YAML section.
The variables will be then accessible in the template. For instance, let's
declare a title and an author (`articles/my-article.md`):

```markdown
---
title: My article
author: Marien
---

This is the content of my article.
```

To access the content of the variables in your template, you need to uppercase
the name of the variable and prepend `ARTICLE_` to it. Let's change our
`templates/article.html` template:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>{{ ARTICLE_TITLE }}</title>
    </head>
    <body>
        <h1>{{ ARTICLE_TITLE }}</h1>
        <p>By {{ ARTICLE_AUTHOR }}</p>

        {{ ARTICLE_CONTENT }}
    </body>
</html>
```

Boop! will then produce the following `_site/my-article.html` file (don't
forget to run the `boop.py` command):

```html
<html>
    <head>
        <title>My article</title>
    </head>
    <body>
        <h1>My article</h1>
        <p>By Marien</p>

        <p>This is the content of my article.</p>
    </body>
</html>
```

## Special variables

The following meta variables have specific significance, especially in the Atom
feed that Boop! generates:

- `title`: the title of the article
- `date`: the publication date of the article
- `update`: the last update date of the article
- `author`: the author of the article
- `slug`: defines the final <abbr>URL</abbr> of the article
- `template`: to override the template file to use

It is highly recommended to always specify `title` and `date`, or default
values will be generated (and they are probably not what you want).

`date` and `update` value must follow this format: `YYYY-MM-DD HH:MM`.

A default `author` value can be set in [the configuration file](/docs/configuration.md).

The default value of `slug` is the basename of the file (e.g. slug of
`my-article.html` is `my-article`, but you can override it).

## Permanent `article` template variables

You'll always be able to access the following variables (as well as your own
variables):

- `ARTICLE_SLUG`: the slug of the article
- `ARTICLE_TEMPLATE`: the name of its template
- `ARTICLE_CONTENT`: the content of the article
- `ARTICLE_SERIE`: [the serie](/docs/series.md) of the article if any
- `ARTICLE_LAST_MODIFICATION`: a special date used by the cache system, don't use it
